# pipeline-justification

## Data extraction

The script `version_recuperation.sh` extracts all versions of a chosen file in a specific branch of a project.

### Usage

The script should be executed at the root of the git project. Make sure the selected branch has been updated.

```
./version_recuperation.sh branch path_to_file path_to_export_folder
```

### Dependencies
- [Git](https://git-scm.com/downloads)

___

## Manual diff extraction

The program `manual_diff.py` creates an Excel workbook containing the details of the deltas between versions contained in a directory.

For YAML files, an automated diff extraction program is available [here](https://github.com/ace-design/pipeline-evolution).

### Installation

1. Make sure you have installed the module [`virtualenv`](https://virtualenv.pypa.io/en/latest/installation.html)

```
python3 -m pip install virtualenv
```

2. Create a virtual environment

```
python3 -m virtualenv venv
```

3. Install program dependencies

```
pip install -r requirements.txt
```

### Usage

Launch the program

```
./manual_diff.py [-h] [-o OUTPUT] directory
```

Close the virtual environment when the extraction is completed

```
deactivate
```

### Dependencies
- [Python 3](https://www.python.org/downloads/)
- [Pip](https://pypi.org/project/pip/)