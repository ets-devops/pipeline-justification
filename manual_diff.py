#!/usr/bin/env python3
import argparse, xlsxwriter
from difflib import context_diff
from os import walk, path


def main(dir, out):
    filenames = next(walk(dir), (None, None, []))[2]
    filenames = [file for file in filenames if file.endswith('.Jenkinsfile') or file.endswith('.Jenkinsfile')]
    filenames.sort()

    with xlsxwriter.Workbook(out) as workbook:
        worksheet = workbook.add_worksheet()
        commits = list(zip(filenames[:-1], filenames[1:]))

        for row, pair in enumerate(commits):
            commit_time, commit_sha, _ = path.basename(pair[1]).rsplit('.', maxsplit=2)

            with open(path.join(dir, pair[0])) as f1, open(path.join(dir, pair[1])) as f2:
                worksheet.write(row, 0, commit_time)
                worksheet.write(row, 1, commit_sha)
                worksheet.write(row, 2, ''.join(list(context_diff(f1.readlines(), f2.readlines(), lineterm='\r\n'))))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog='manual_diff',
        description='Creates a xlsx file containing detailed diffs between all files in directory')
    parser.add_argument('-o', '--output', help='output filename', default='diffs.xlsx')
    parser.add_argument('directory')

    args = parser.parse_args()
    main(args.directory, args.output)

