#!/bin/bash

# based on script by Dmitry Shevkoplyas and revisions by Nathan Arthur at https://stackoverflow.com/questions/12850030/git-getting-all-previous-version-of-a-specific-file-folder

#set -e

# take relative path to the file to inspect
BRANCH=$1
FILE_PATH=$2
EXPORT_TO=$3

display_usage() {
    echo -e "\nExtract all versions of FILE in git BRANCH to DIRECTORY\n"
    echo "This script must be run at the root of your git project. Make sure the BRANCH has been downloaded localy."
    echo -e "\nUsage: $0 BRANCH FILE DIRECTORY\n"
}

# check if got argument
if [ "${BRANCH}" == "" ] || [ "${FILE_PATH}" == "" ] || [ "${EXPORT_TO}" == "" ]; then
    echo "Error: wrong arguments given." >&2
    display_usage
    exit 1
fi

# check if local branch exists
# if branch is currently selected, the star from git branch lists all files, but it's okay for now
if ! git branch -l "${BRANCH}" | grep -w "${BRANCH}" ; then
    echo "Error: branch not found. Make sure to fetch remote version before running this script" >&2
    display_usage
    exit 1
else
    git checkout "${BRANCH}"
fi

# check if file exist
if [ ! -f ${FILE_PATH} ]; then
    echo "error: File '${FILE_PATH}' does not exist." >&2
    display_usage
    exit 1
fi

# extract just a filename from given relative path (will be used in result file names)
GIT_SHORT_FILENAME=$(basename $FILE_PATH)

# create folder to store all revisions of the file
if [ ! -d ${EXPORT_TO} ]; then
    echo "creating folder: ${EXPORT_TO}"
    mkdir ${EXPORT_TO}
fi

# creating log file
LOGS=${EXPORT_TO}/'file_logs.csv'
echo "Commit Hash,Date,Author Name,Author Email,Commit Message" > ${LOGS}

# catalog all file revisions
echo "Writing files to '$EXPORT_TO'"
git log --diff-filter=d --date-order --reverse --format="%ad %H" --date=iso-strict "$FILE_PATH" | grep -v '^commit' | \
    while read LINE; do \
        COMMIT_DATE=`echo $LINE | cut -d ' ' -f 1 | tr ':' '.'` ; \
        echo "$COMMIT_DATE"; \
        COMMIT_SHA=`echo $LINE | cut -d ' ' -f 2`; \
        printf '.' ; \
        git cat-file -p "$COMMIT_SHA:$FILE_PATH" > "$EXPORT_TO/$COMMIT_DATE.$COMMIT_SHA.$GIT_SHORT_FILENAME" ; \
        git show ${COMMIT_SHA} --format='%H,%as,%an,%ae,%s' | head -n1 >> ${LOGS}
    done
echo

# return success code
echo "result stored to ${EXPORT_TO}"
exit 0
